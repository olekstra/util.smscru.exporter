﻿namespace Util.SmscRu.Exporter
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    public static class Program
    {
        public static async Task Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false)
                .AddCommandLine(args)
                .AddUserSecrets(typeof(Program).Assembly)
                .Build();

            var services = new ServiceCollection()
                .AddLogging(o => o.AddConfiguration(config.GetSection("Logging")).AddConsole())
                .AddSingleton<IConfiguration>(config)
                .Configure<SmscOptions>(config.GetSection("SmscOptions"))
                .AddHttpClient<IExportService, ExportService>().Services
                .BuildServiceProvider();

            var loggerFactory = services.GetRequiredService<ILoggerFactory>();
            var logger = loggerFactory.CreateLogger(nameof(Program));

            logger.LogDebug("Starting...");

            var previousMonth = DateTime.Now.AddMonths(-1);
            var year = config.GetValue<int?>("year") ?? previousMonth.Year;
            var month = config.GetValue<int?>("month") ?? previousMonth.Month;
            var sender = config.GetValue<string>("sender");
            var day = config.GetValue<int?>("day") ?? 1;

            logger.LogInformation($"Year={year}, Month={month} (from day {day}), Sender='{sender}'");

            var exporter = services.GetRequiredService<IExportService>();
            await exporter.Export(year, month, day, sender).ConfigureAwait(false);

            logger.LogInformation("DONE");
            await Task.Delay(500); // waiting for logging to complete
        }
    }
}
