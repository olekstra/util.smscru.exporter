﻿namespace Util.SmscRu.Exporter
{
    using System;
    using System.Threading.Tasks;

    public interface IExportService
    {
        Task Export(int year, int month, int day, string sender);
    }
}
