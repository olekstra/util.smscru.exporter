﻿namespace Util.SmscRu.Exporter
{
    using Newtonsoft.Json;

    public class SmscRecord
    {
        public int status { get; set; }
        public string last_date { get; set; }
        public int last_timestamp { get; set; }
        public string send_date { get; set; }
        public int send_timestamp { get; set; }
        public string phone { get; set; }
        public string cost { get; set; }
        public string sender_id { get; set; }
        public string status_name { get; set; }
        public string message { get; set; }
        public string mccmnc { get; set; }
        public string country { get; set; }
        [JsonProperty("operator")]
        public string operator_act { get; set; }
        public string operator_orig { get; set; }
        public string region { get; set; }
        public int type { get; set; }
        public int id { get; set; }
        public long int_id { get; set; }
        public int sms_cnt { get; set; }
        public int format { get; set; }
        public int crc { get; set; }
    }
}
