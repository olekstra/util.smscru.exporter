﻿using System.Linq;

namespace Util.SmscRu.Exporter
{
    public class OutputRecord
    {
        public OutputRecord()
        {
            // Nothing
        }

        public OutputRecord(SmscRecord smsc)
        {
            var sd = smsc.send_date.Split(' ', 2);
            this.Date = sd[0];
            this.Time = sd[1];
            this.Phone = string.Concat(smsc.phone.Select((x, i) => i > 3 && i <= 6 ? '*' : x));
            this.Text = smsc.message;
            this.Parts = smsc.sms_cnt;
            this.Status = smsc.status_name;
        }

        public string Date { get; set; }

        public string Time { get; set; }

        public string Phone { get; set; }

        public string Text { get; set; }

        public int Parts { get; set; }

        public string Status { get; set; }
    }
}
