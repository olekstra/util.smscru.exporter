﻿namespace Util.SmscRu.Exporter
{
    public class SmscOptions
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}
