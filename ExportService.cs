﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Util.SmscRu.Exporter
{
    public class ExportService : IExportService
    {
        private readonly HttpClient httpClient;
        private readonly SmscOptions options;
        private readonly ILogger<ExportService> logger;

        public ExportService(HttpClient httpClient, IOptions<SmscOptions> options, ILogger<ExportService> logger)
        {
            this.httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            this.options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));

            httpClient.Timeout = TimeSpan.FromMinutes(10);

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        public async Task Export(int year, int month, int day, string sender)
        {
            var data = new List<OutputRecord>();
            long? prev_id = null;

            var startDate = new DateTime(year, month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);

            if (day != 1)
            {
                endDate = new DateTime(startDate.Year, startDate.Month, day);
            }

            var startDateText = startDate.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
            var endDateText = endDate.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);

            logger.LogInformation($"Starting export from {startDateText} to {endDateText} for login '{options.Login}' and sender_id '{sender}'");

            while (true)
            {
                var url = $"https://smsc.ru/sys/get.php?get_messages=1&login={options.Login}&psw={options.Password}&start={startDateText}&end={endDateText}&cnt=1000&fmt=3&prev_id={prev_id}";
                var text = await httpClient.GetStringAsync(url).ConfigureAwait(false);

                if (text.StartsWith("{"))
                {
                    logger.LogDebug(text);

                    var error = JsonConvert.DeserializeObject<ErrorResponse>(text);

                    switch (error.ErrorCode)
                    {
                        case 3: // message not found
                            logger.LogDebug("No more records.");
                            break;

                        default:
                            logger.LogWarning($"Unexpected error {error.ErrorCode}: '{error.Error}', saving incomplete file");
                            break;
                    }

                    break;
                }

                var items = JsonConvert.DeserializeObject<SmscRecord[]>(text);
                if (items.Length == 0)
                {
                    logger.LogDebug("No more records.");
                    break;
                }

                prev_id = items.Min(x => x.int_id);

                var selector = string.IsNullOrEmpty(sender) ? items : items.Where(x => x.sender_id.Contains(sender, StringComparison.InvariantCultureIgnoreCase));
                data.AddRange(selector.Select(x => new OutputRecord(x)));

                logger.LogDebug($"Loaded {items.Length} more records (prev_id now {prev_id}), having {data.Count} filtered records");
            }

            var fileName = $"smsc-{sender}-{year}-{month:00}--{DateTimeOffset.Now.Ticks}.csv";

            logger.LogInformation($"Saving {data.Count} items to '{fileName}'...");

            using var file = new StreamWriter(fileName);
            var config = new CsvConfiguration(CultureInfo.InvariantCulture) { Delimiter = ";" };
            using var writer = new CsvWriter(file, config);
            writer.WriteRecords(data);
        }

        private class ErrorResponse
        {
            [JsonPropertyName("error")]
            public string Error { get; set; }

            [JsonPropertyName("error_code")]
            public int ErrorCode { get; set; }
        }
    }
}
